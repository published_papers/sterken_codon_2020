################################################################################
###Load folding data & write to df



################################################################################
###Load target file

    target.file <- read.delim(file="./Structures_Vienna_raw/Target_file.txt",header=T)
    target.file <- target.file[target.file$Algorithm == "Andronescu2007" & target.file$Temperature ==20,]





################################################################################
###Load data, Arabidopsis: folding

    ###Match data
    setwd("./Match_files")
        aff.to.genbank <- read.delim(file="Arabidopsis_Aff.txt"); head(aff.to.genbank)
        genbank.to.refseq <- read.delim(file="TAIR10_NCBI_REFSEQ_mapping_RNA",header=F); head(genbank.to.refseq)
        refseq.to.araid <- read.delim(file="TAIR10_Model_cDNA_associations"); head(refseq.to.araid)
    setwd("../")

    for(i in 1:sum(target.file[,1]=="Arabidopsis")){
        ###Go through it by target file
        current.data <- as.matrix(target.file[target.file[,1]=="Arabidopsis",][i,])

        ###Load counted data
        setwd("./Structures_Vienna_processed/")
            load(file=paste("obj_",current.data[1],"_",current.data[2],"_CDS_",current.data[4],"_",current.data[3],"_counted.out",sep=""))
            head(counted.file)
        setwd("../")


        ###Gene expression data has array ID names
        ###Protein abundance data has AT names
        identifiers <- merge(genbank.to.refseq,refseq.to.araid,by.x=3,by.y=1) %>%
                       merge(aff.to.genbank,by.x=2,by.y=2)
            colnames(identifiers) <- c("x1","Protein","x2","CDS","x3","Probe")

        ###Names from folding data
        name.tmp <- unlist(strsplit(as.character(counted.file[,1]),split="|",fixed=T))
            names.nu <- name.tmp[which(name.tmp == "gb")+1]; length(names.nu)-nrow(counted.file)

        counted.file <- cbind(gene.name=names.nu,counted.file[,-1])

        ara.file <- select(identifiers,Probe,Protein,CDS) %>%
                    merge(counted.file,by.x=3,by.y=1)
    }


    rm(list=c("aff.to.genbank","genbank.to.refseq","refseq.to.araid"))



################################################################################
###Load data, C. elegans: folding

    ###Match data
    setwd("./Match_files")
        aff.to.WBID <- read.delim(file="c_elegans.PRJNA13758.WS241.affy_oligo_mapping.txt"); head(aff.to.WBID)
    setwd("../")

    for(i in 1:sum(target.file[,1]=="C. elegans")){
        ###Go through it by target file
        current.data <- as.matrix(target.file[target.file[,1]=="C. elegans",][i,])

        ###Load counted data
        setwd("./Structures_Vienna_processed/")
            load(file=paste("obj_",current.data[1],"_",current.data[2],"_CDS_",current.data[4],"_",current.data[3],"_counted.out",sep=""))
            head(counted.file)
        setwd("../")


        ###Gene expression data has array ID names
        ###Protein abundance data has sequence names
        identifiers <- select(aff.to.WBID,Oligo_set,Gene_sequence_name,WBGeneID) %>%
                       rename(Probe=Oligo_set,Protein=Gene_sequence_name,CDS=WBGeneID)

        ###Names from folding data
        name.tmp <- unlist(strsplit(as.character(counted.file[,1]),split="=",fixed=T))
            names.nu <- name.tmp[seq(2,length(name.tmp),by=2)]; length(names.nu)-nrow(counted.file)

        counted.file <- cbind(gene.name=names.nu,counted.file[,-1])

        ele.file <- merge(identifiers,counted.file,by.x=3,by.y=1)

    }

    rm("aff.to.WBID")


################################################################################
###Load data, Mouse: folding

    ###Match data
    setwd("./Match_files")
        aff.to.gene <- read.delim(file="Mouse_Aff.txt"); head(aff.to.gene)
        CCDS.to.gene <- read.delim(file="Mouse_CCDS.txt"); head(CCDS.to.gene)
        ensembl <- read.delim(file="Mouse_ensembl.txt"); head(ensembl)
    setwd("../")

    for(i in 1:sum(target.file[,1]=="Mouse")){
        ###Go through it by target file
        current.data <- as.matrix(target.file[target.file[,1]=="Mouse",][i,])

        ###Load counted data
        setwd("./Structures_Vienna_processed/")
            load(file=paste("obj_",current.data[1],"_",current.data[2],"_CDS_",current.data[4],"_",current.data[3],"_counted.out",sep=""))
            head(counted.file)
        setwd("../")


        ###Gene expression data has array ID names
        ###Protein abundance data has sequence names
        identifiers <- merge(aff.to.gene,CCDS.to.gene,by.x=10,by.y=2) %>%
                       merge(ensembl,by.x=11,by.y=5) %>%
                       filter(!duplicated(ID)) %>%
                       select(ID,Ensembl.Protein.ID,ccds_id) %>%
                       rename(Probe=ID,Protein=Ensembl.Protein.ID,CDS=ccds_id)

        ###Names from folding data
        name.tmp <- unlist(strsplit(as.character(counted.file[,1]),split=">",fixed=T))
        name.tmp <- name.tmp[seq(2,length(name.tmp),by=2)]
        name.tmp <- unlist(strsplit(as.character(name.tmp),split="|",fixed=T))
            names.nu <- name.tmp[seq(1,length(name.tmp),by=3)]; length(names.nu)-nrow(counted.file)

        counted.file <- cbind(gene.name=names.nu,counted.file[,-1])

        mus.file <- merge(identifiers,counted.file,by.x=3,by.y=1)
    }
    rm(list=c("aff.to.gene","CCDS.to.gene","ensembl"))



################################################################################
###Load data, Yeast: folding

    ###Match data
    setwd("./Match_files")
        aff.to.gene <- read.delim(file="Yeast_Aff.txt"); head(aff.to.gene)
    setwd("../")

    for(i in 1:sum(target.file[,1]=="Yeast")){
        ###Go through it by target file
        current.data <- as.matrix(target.file[target.file[,1]=="Yeast",][i,])

        ###Load counted data
        setwd("./Structures_Vienna_processed/")
            load(file=paste("obj_",current.data[1],"_",current.data[2],"_CDS_",current.data[4],"_",current.data[3],"_counted.out",sep=""))
            head(counted.file)
        setwd("../")

        ###Gene expression data has array ID names
        ###Protein abundance data has sequence names
        identifiers <- select(aff.to.gene,ID,ORF) %>%
                       mutate(ORF2=ORF) %>%
                       rename(Probe=ID,Protein=ORF,CDS=ORF2)

        ###Names from folding data
        name.tmp <- unlist(strsplit(as.character(counted.file[,1]),split=",",fixed=T))
        name.tmp <- name.tmp[grep(">",name.tmp)]
            ###cleanup needed
            name.tmp <- name.tmp[-grep("<i>",name.tmp)]
            name.tmp <- name.tmp[-grep("<sub>",name.tmp)]
            name.tmp <- name.tmp[-grep("<sup>",name.tmp)]
            name.tmp <- name.tmp[-grep("-->5",name.tmp)]

        name.tmp <- unlist(strsplit(as.character(name.tmp),split=" ",fixed=T))
        name.tmp <- name.tmp[grep(">",name.tmp)]
        name.tmp <- unlist(strsplit(as.character(name.tmp),split=">",fixed=T))
        names.nu <- name.tmp[seq(2,length(name.tmp),by=2)]

        counted.file <- cbind(gene.name=names.nu,counted.file[,-1])

        yea.file <- merge(identifiers,counted.file,by.x=3,by.y=1)

    }
    rm(list=c("aff.to.gene"))


################################################################################
###Load data, E.coli: folding

    ###Match data
    setwd("./Match_files")
        aff.to.genbank <- read.delim(file="Ecoli_Aff.txt"); head(aff.to.genbank)
    setwd("../")

    for(i in 1:sum(target.file[,1]=="E.coli" & target.file[,2]=="MG1665")){
        ###Go through it by target file
        current.data <- as.matrix(target.file[target.file[,1]=="E.coli" & target.file[,2]=="MG1665",][i,])

        ###Load counted data
        setwd("./Structures_Vienna_processed/")
            load(file=paste("obj_",current.data[1],"_",current.data[2],"_CDS_",current.data[4],"_",current.data[3],"_counted.out",sep=""))
            head(counted.file)
        setwd("../")

        ###Gene expression data has array ID names
        ###Protein abundance data has sequence names
        identifiers <- aff.to.genbank
        identifiers.tmp <- unlist(strsplit(as.character(identifiers[,8]),split="_",fixed=T))

        identifiers <- cbind(identifiers[,1],NA,identifiers[,c(8,2)])
        identifiers[sort(c(grep("CFT073",identifiers[,3]),grep("EDL933",identifiers[,3]),grep("MG1655",identifiers[,3]),grep("SAKAI",identifiers[,3]),grep("IG",identifiers[,3]))),2] <- identifiers.tmp[sort(c(grep("CFT073",identifiers.tmp),grep("EDL933",identifiers.tmp),grep("MG1655",identifiers.tmp),grep("SAKAI",identifiers.tmp),grep("IG",identifiers.tmp)))+1]
        identifiers <- identifiers[,c(1,4,2)]
            colnames(identifiers) <- c("Probe","Protein","CDS")


        ###Names from folding data
        name.tmp <- unlist(strsplit(as.character(counted.file[,1]),split="[",fixed=T))
            name.tmp <- unlist(strsplit(as.character(name.tmp),split="]",fixed=T))
            name.tmp <- name.tmp[grep("gene=",name.tmp)]
            name.tmp <- unlist(strsplit(as.character(name.tmp),split="gene=",fixed=T))
        names.nu <- name.tmp[seq(2,length(name.tmp),by=2)]; length(names.nu)-nrow(counted.file)

        counted.file <- cbind(gene.name=names.nu,counted.file[,-1])

        col.file <- merge(identifiers,counted.file,by.x=3,by.y=1)

    }
    rm(list=c("aff.to.genbank","current.data","counted.file","identifiers","identifiers.tmp","name.tmp","names.nu"))


################################################################################
###compile and store dataframe

    ###Merge the species together
        folded_counted <- rbind(cbind(species="A. thaliana",ara.file),
                                cbind(species="C. elegans",ele.file),
                                cbind(species="M. musculus",mus.file),
                                cbind(species="S. cerevisiae",yea.file),
                                cbind(species="E. coli",col.file))


    ###Make sure column values are in the right format
        for(i in 1:ncol(folded_counted)){
            if(i %in% 1:4){
                folded_counted[,i] <- as.character(unlist(folded_counted[,i]))
            }
            if(i %in% 5:ncol(folded_counted)){
                folded_counted[,i] <- as.numeric(as.character(unlist(folded_counted[,i])))
            }
        }


    ###Add the positional statistics and a diversity index
        folded_counted <- filter(folded_counted,!duplicated(paste(Probe,species,CDS,Protein))) %>%
                          group_by(Probe,species,CDS,Protein) %>%
                          mutate(A_all=A.1+A.2+A.3,T_all=T.1+T.2+T.3,C_all=C.1+C.2+C.3,G_all=G.1+G.2+G.3) %>%
                          gather(key=codon,value=count,-c(species:min.loop.size,A.1:G_all)) %>%
                          mutate(diversity=(sum(count!=0))/gene.length) %>%
                          group_by(Probe,species,CDS,Protein) %>%
                          spread(key=codon,value=count) %>%
                          as.data.frame()


    ###Count the amino acid abundances
        AA.trip <- cbind(codon=triplets,amino=AA)

        addon.nu <- NULL
        for(i in 1:length(unique(AA.trip[,2]))){
            selection <- which(colnames(folded_counted) %in% AA.trip[AA.trip[,2] == unique(AA.trip[,2])[i]])
            if(length(selection)>1){
                addon.nu <- cbind(addon.nu,apply(matrix(as.numeric(as.character(unlist(folded_counted[,selection]))),ncol=length(selection)),1,sum))
            }
            if(length(selection)==1){
                addon.nu <- cbind(addon.nu,as.numeric(as.character(unlist(folded_counted[,selection]))))
            }
        }
        colnames(addon.nu) <- paste("AA_",unique(AA.trip[,2]),sep="")

        folded_counted <- cbind(folded_counted,addon.nu)


    ###Count the relative usage per codon
        addon.nu <- NULL
        for(i in 1:length(unique(AA.trip[,2]))){
            selection <- which(colnames(folded_counted) %in% AA.trip[AA.trip[,2] == unique(AA.trip[,2])[i],1])
            if(length(selection)>1){
                addon.nu <- cbind(addon.nu,matrix(as.numeric(as.character(unlist(folded_counted[,selection]))),ncol=length(selection))/apply(matrix(as.numeric(as.character(unlist(folded_counted[,selection]))),ncol=length(selection)),1,sum))
            }
            if(length(selection)==1){
                addon.nu <- cbind(addon.nu,as.numeric(as.character(unlist(folded_counted[,selection])))/as.numeric(as.character(unlist(folded_counted[,selection]))))
            }
            colnames(addon.nu)[((ncol(addon.nu)-length(selection))+1):ncol(addon.nu)] <- paste("relative",unique(AA.trip[,2])[i],colnames(folded_counted)[selection],sep="_")
        }

        folded_counted <- cbind(folded_counted,addon.nu)


    ###Express some of the stats as relative to the number of nucleotides
    folded_counted <- mutate(folded_counted,
                             structure_relative_bound.nuc=bound.nuc/gene.length,
                             structure_relative_free.nuc=free.nuc/gene.length,
                             structure_relative_energy=energy/gene.length,
                             structure_relative_no.stems=no.stems/gene.length,
                             structure_relative_no.loops=no.loops/gene.length,
                             structure_relative_A.1=A.1/gene.length,
                             structure_relative_C.1=C.1/gene.length,
                             structure_relative_G.1=G.1/gene.length,
                             structure_relative_T.1=T.1/gene.length,
                             structure_relative_A.2=A.2/gene.length,
                             structure_relative_C.2=C.2/gene.length,
                             structure_relative_G.2=G.2/gene.length,
                             structure_relative_T.2=T.2/gene.length,
                             structure_relative_A.3=A.3/gene.length,
                             structure_relative_C.3=C.3/gene.length,
                             structure_relative_G.3=G.3/gene.length,
                             structure_relative_T.3=T.3/gene.length,
                             structure_relative_A.all=A_all/gene.length,
                             structure_relative_C.all=C_all/gene.length,
                             structure_relative_G.all=G_all/gene.length,
                             structure_relative_T.all=T_all/gene.length) %>%
                      rename(structure_absolute_gene.length=gene.length,
                             structure_absolute_bound.nuc=bound.nuc,
                             structure_absolute_free.nuc=free.nuc,
                             structure_absolute_energy=energy,
                             structure_absolute_R2s=R2s,
                             structure_absolute_no.stems=no.stems,
                             structure_absolute_av.stem.size=av.stem.size,
                             structure_absolute_med.stem.size=med.stem.size,
                             structure_absolute_sd.stem.size=sd.stem.size,
                             structure_absolute_max.stem.size=max.stem.size,
                             structure_absolute_min.stem.size=min.stem.size,
                             structure_absolute_no.loops=no.loops,
                             structure_absolute_av.loop.size=av.loop.size,
                             structure_absolute_med.loop.size=med.loop.size,
                             structure_absolute_sd.loop.size=sd.loop.size,
                             structure_absolute_max.loop.size=max.loop.size,
                             structure_absolute_min.loop.size=min.loop.size,
                             structure_absolute_A.1=A.1,
                             structure_absolute_C.1=C.1,
                             structure_absolute_G.1=G.1,
                             structure_absolute_T.1=T.1,
                             structure_absolute_A.2=A.2,
                             structure_absolute_C.2=C.2,
                             structure_absolute_G.2=G.2,
                             structure_absolute_T.2=T.2,
                             structure_absolute_A.3=A.3,
                             structure_absolute_C.3=C.3,
                             structure_absolute_G.3=G.3,
                             structure_absolute_T.3=T.3,
                             structure_absolute_A.all=A_all,
                             structure_absolute_C.all=C_all,
                             structure_absolute_G.all=G_all,
                             structure_absolute_T.all=T_all,
                             structure_absolute_diversity=diversity) %>%
                      select(species:Protein,structure_absolute_gene.length:structure_absolute_diversity,structure_relative_bound.nuc:structure_relative_T.all,AAA:AA_G,relative_F_TTC:relative_G_GGT)


    ###Again, make sure the format is good
        for(i in 1:ncol(folded_counted)){
            if(i %in% 1:4){
                folded_counted[,i] <- as.character(unlist(folded_counted[,i]))
            }
            if(i %in% 5:ncol(folded_counted)){
                folded_counted[,i] <- as.numeric(as.character(unlist(folded_counted[,i])))
            }
        }


    ###Save it
        save(folded_counted,file="obj_folded_counted.out")


setwd("../")






