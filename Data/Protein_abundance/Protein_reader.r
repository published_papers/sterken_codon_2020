################################################################################
###Load protein abundance data & write to df


setwd("./PaxDB4.0")

files <- dir()
files <- files[grepl(".txt",files)]

###Load data
    protein_abundance <- NULL
    for(i in 1:length(files)){
        collection <- read.delim(file=files[i],skip=10)
        collection <- as.data.frame(cbind(type="protein_abundance",
                                          species=ifelse(grepl("thaliana",files[i]),"A. thaliana",
                                                    ifelse(grepl("elegans",files[i]),"C. elegans",
                                                      ifelse(grepl("E.coli",files[i]),"E. coli",
                                                        ifelse(grepl("musculus",files[i]),"M. musculus",
                                                          ifelse(grepl("S.cerevisiae",files[i]),"S. cerevisiae",NA))))),
                                          collection)) %>%
                      mutate(Identifier=ifelse(species=="A. thaliana",gsub("3702.","",string_external_id),
                                          ifelse(species=="C. elegans",gsub("6239.","",string_external_id),
                                            ifelse(species=="E. coli",gsub("511145.","",string_external_id),
                                              ifelse(species=="M. musculus",gsub("10090.","",string_external_id),
                                                ifelse(species=="S. cerevisiae",gsub("4932.","",string_external_id),NA)))))) %>%
                      select(species,Identifier,type,abundance)


        protein_abundance  <- rbind(protein_abundance,collection)
    }

###Make sure format is good
    for(i in 1:ncol(protein_abundance)){
        if(i %in% 1:3){
            protein_abundance[,i] <- as.character(unlist(protein_abundance[,i]))
        }
        if(i %in% 4){
            protein_abundance[,i] <- as.numeric(as.character(unlist(protein_abundance[,i])))
        }
    }

###Add rank data
    protein_abundance <- group_by(protein_abundance,type,species) %>%
                         filter(abundance != 0) %>%
                         mutate(rank_expr=rank(abundance,ties.method="average")/length(abundance)*100) %>%
                         mutate(abundance=log2(abundance+1)) %>%
                         mutate(protein_zscore=(abundance-mean(abundance,na.rm=T))/sd(abundance,na.rm=T)) %>%
                         data.frame()





save(protein_abundance,file="../obj_protein_abundance.out")

rm(list=c("files","i","collection"))

setwd("../")



